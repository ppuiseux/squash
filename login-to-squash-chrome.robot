*** Settings ***
Documentation     Tout pour un test de login sur Squash TM 3.x
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary

*** Variables ***
${SERVER}         nxqlx039-2:8080/squash
${BROWSER}        Chrome
${DELAY}          0
${VALID USER}     admin
${VALID PASSWORD}    admin
${LOGIN URL}      http://${SERVER}/login
${WELCOME URL}    http://${SERVER}/home-workspace
${TEST_CASE_URL}  http://${SERVER}/administration-workspace/system/messages
${REQ_URL}        http://${SERVER}/requirement-workspace
${ERROR URL}      http://${SERVER}/login

*** Keywords ***
Open Browser To Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Login Page Should Be Open

Login Page Should Be Open
    Title Should Be    Squash Test Management

Test Cases Page Should Be Open
    Wait Until Page Contains    Message de la page de login

Req Page Should Be Open
    Wait Until Location Is    ${REQ_URL}

Go To Login Page
    Go To    ${LOGIN URL}
    Login Page Should Be Open

Go To Test Cases Page
    Go To    ${TEST_CASE_URL}
    Wait Until Location Is     ${TEST_CASE_URL} 
    Test Cases Page Should Be Open

Go To Req Page
    Go To    ${REQ_URL}
    Req Page Should Be Open

Input Username
    [Arguments]    ${username}
    Input Text    xpath=//input[@formcontrolname='login']    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    xpath=//input[@formcontrolname='password']    ${password}

Submit Credentials
    Click Button    submit-login-form
    Wait Until Location Is    ${WELCOME URL}

Welcome Page Should Be Open 
    Location Should Be    ${WELCOME URL}
    Title Should Be    Squash Test Management

*** Test Cases ***
Valid Login
    Open Browser To Login Page
    Input Username    admin
    Input Password    admin
    Submit Credentials
    Welcome Page Should Be Open

Ouverture Cas de Tests
    Go To Test Cases Page
    Test Cases Page Should Be Open

Ouverture Req
    Go To Req Page
    Req Page Should Be Open
    [Teardown]    Close Browser

